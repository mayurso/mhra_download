# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 11:09:21 2018

@author: Mayur Sonawane
"""


import scrapy
from medmeme_spyder.utilities import s3_exists, get_s3_client, get_logger
import datetime
import os
from scrapy.http import Request

logger = get_logger('mhraSpiderLog')


class mhraSpider(scrapy.spider):
    name = "mhraSpider"


    def __init__(self):
        super(mhraSpider, self).__init__()
        self.start_urls = ["http://www.mhra.gov.uk/spc-pil/index.htm?secLevelIndexChar=Av%20-%20Az#retainDisplay"]
        self.allowed_domains = ["http://www.mhra.gov.uk"]
        self.s3_client = get_s3_client()
        self.bucket = 'mmcapturepoctemp'
        self.s3_root = os.path.join('data', datetime.datetime.now().strftime("%Y/%m/%d"), 'unstructured', 'raw', 'ema')
        self.upload_to_s3 = False

    def parse(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "indexChar" in href:
                print("-----level_1_big_alphabets--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_small_alphabets
                )



    def parse_small_alphabets(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "secLevelIndexChar" in href:
                print("-----level_2_small_alphabets--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_subs
                )



    def parse_subs(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "subsName" in href:
                print("-----subs--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_products
                )



    def parse_products(self, response):
        current_address_url = response.request.url
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "prodName" in href and href not in current_address_url:
                print("-----products--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_article
                )



    def parse_article(self, response):
        for href in response.css('a[href$=".pdf"]::attr(href)').extract():
            print("***********PDF_________________", href)
            if "/home/groups/spcpil/documents/spcpil/" in href:
                yield Request(
                    url=response.urljoin(href),
                    callback=self.save_pdf
                )


    def save_pdf(self, response):
        filename = response.url.replace('/', '~')
        s3_file_key = os.path.join(self.s3_root, filename)
        location = "/Users/bharatsamudrala/PycharmProjects/medmeme-scrapy-crawler/ema/"  # get_temp_download_directory()
        file_location = os.path.join(location, filename)
        if response.status == 200:

            if not s3_exists(self.s3_client, self.bucket, s3_file_key):
                self.upload_to_s3 = True

            with open(file_location, 'wb') as f:
                f.write(response.body)

            if self.upload_to_s3:
                self.s3_client.upload_file(
                    file_location,
                    self.bucket,
                    s3_file_key)
                logger.info('Moved{}toS3'.format(filename))
                # os.remove(filename)
                # logger.info(‘Removed {} from local’ .format(filename))
        else:
            logger.info('Not a 200  status - filename {}'.format(filename))
            logger.info('** ** ** STATUS CODE {} and url {}'.format(response.status, response.request.url))


